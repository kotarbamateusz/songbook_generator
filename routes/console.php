<?php

use App\SongSearch;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('songbook:search-title {title}', function ($title) {
    $msg = SongSearch::searchByTitle($title);
    $this->comment($msg);
})->describe('Display an inspiring quote');


Artisan::command('songbook:search-title-author {title} {author}', function ($title, $author = null) {
    $msg = SongSearch::searchByTitleAuthor($title, $author);
    $this->comment($msg);
})->describe('Display an inspiring quote');
