<?php

namespace App;

use App\SongPage\Wywrota;
use Illuminate\Database\Eloquent\Model;

class SongSearch extends Model
{
    public static function searchByTitleAuthor($title, $author)
    {
        self::msg('Search song by title: '.$title.' and author: '.$author);

    }

    public static function searchByTitle($title)
    {
        self::msg('Search song by title: '.$title);

        $pageSearchClass = new Wywrota();
        $list = $pageSearchClass->getSongLists($title);
        dd($list);
    }

    private static function msg($msg)
    {
        echo $msg."\n";
    }
}
