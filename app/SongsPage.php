<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongsPage extends Model
{
    protected $fillable = ['page_url'];
}
