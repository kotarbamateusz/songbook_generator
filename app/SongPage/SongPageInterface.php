<?php

namespace App\SongPage;

interface SongPageInterface
{
    /** 
     * Get model name like 'ultimate_guitar"
     *
     * @return string
     */
    public function getModelName();

    /** 
     * Get domain name like 'https://tabs.ultimate-guitar.com'
     *
     * @return string
     */
    public function getDomain();

    /**
     * Get list from content as array
     * 
     * @return array
     * [
     *  [0 => URL, 1 => Author, 2 => Title]
     * ]
     */ 
    public function getListFromContent(string $content);

    /** 
     * Get songs list
     * of songs with url
     * [
     *  0 => ['title' => TITLE, 'url' => 'http://example.com/my-song']
     * ]
     *
     * @return array 
     */
    public function getSongLists($title, $author = null);

    /** 
     * Prepare url to search song by song and option author
     * 
     * @return string
     */
    public function prepareSearchUrl($title, $author = null);

    /** 
     * Get song content by page url
     * get only content of that song text + chords
     * 
     * @return string
     */
    public function getSongContent($url);

    /** 
     * Return song title from content
     * 
     * @return string
     */
    public function getSongTitle();
}
