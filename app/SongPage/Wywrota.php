<?php

namespace App\SongPage;

use App\SongPage;
use App\SongsPage;
use App\Page\FileGetContents;
use App\SongPage\SongPageInterface;

class Wywrota implements SongPageInterface 
{
    /** 
     * Get model name like 'wywrota"
     *
     * @return string
     */
    public function getModelName()
    {
        return 'wywrota';
    }

    /** 
     * Get domain name like 'https://tabs.ultimate-guitar.com'
     *
     * @return string
     */
    public function getDomain()
    {
        return 'https://spiewnik.wywrota.pl';
    }

    /** 
     * Get songs list
     * of songs with url
     * [
     *  0 => ['title' => TITLE, 'url' => 'http://example.com/my-song']
     * ]
     *
     * @return array 
     */
    public function getSongLists($title, $author = null)
    {
        $url = $this->prepareSearchUrl($title, $author);
        $content = FileGetContents::get($url);

        $records = $this->getListFromContent($content);

        foreach ($records as $record)
        {
            $url = $record[0];
            $author = $record[1];
            $title = $record[2];

            $content = FileGetContents::get($url);



            $songPage = SongsPage::firstOrCreate([
                'page_url' => $url
            ]);
            $songPage->page_domain = $this->getModelName();
            $songPage->page_status = 200;
            $songPage->page_content = $content; 
            dd($songPage->page_url);
            $songPage->save();
        }
    }

    /**
     * Get list from content as array
     * 
     * @return array
     * [
     *  [0 => URL, 1 => Author, 2 => Title]
     * ]
     */ 
    public function getListFromContent(string $content)
    {
        $list = [];
        $re = '/list-group-item">\s+<a[^>]+href="([^>]+)">\s+([^-]+)-(\s+[^<]+)<\/a>/m';
        preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

        // Print the entire match result
        if (!empty($matches))
        {
            foreach ($matches as $match)
            {
                $list[] = [
                    0 => trim($match[1]),
                    1 => trim($match[2]),
                    2 => trim($match[3]),
                ];
            }
        }

        return $list;
    }

    /** 
     * Prepare url to search song by song and option author
     * https://spiewnik.wywrota.pl/search?q=whiskey
     * 
     * @return string
     */
    public function prepareSearchUrl($title, $author = null)
    {
        $title = str_replace(' ', '+', $title);
        $title = strtolower($title);
        return $this->getDomain().'/search?q='.$title;
    }

    /** 
     * Get song content by page url
     * get only content of that song text + chords
     * 
     * @return string
     */
    public function getSongContent($url)
    {

    }

    /** 
     * Return song title from content
     * 
     * @return string
     */
    public function getSongTitle()
    {

    }
}