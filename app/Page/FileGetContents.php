<?php

namespace App\Page;

use App\Page\DownloaderInterface;
use Illuminate\Support\Facades\Cache;

class FileGetContents implements DownloaderInterface
{
    /** 
     * Need to cache records!
     */
    public static function get($url)
    {
        $content = Cache::get($url);
        if (!empty($content))
        {
            return $content;
        }

        $content = file_get_contents($url);
        Cache::put($url, $content, 86400);
        return $content;
    }
}