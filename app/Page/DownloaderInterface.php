<?php

namespace App\Page;

interface DownloaderInterface
{
    /** 
     * Need to cache records!
     */
    public static function get($url);
}