<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSongsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs_search', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist', 256)->nullable();
            $table->string('title', 256)->nullable();
            $table->timestamps();
        });

        Schema::create('songs_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('page_domain', 256)->nullable();
            $table->string('page_url', 2048)->nullable();
            $table->smallInteger('page_status')->nullable();
            $table->text('page_content')->nullable();
            $table->timestamps();

            $table->unsignedInteger('song_search_id')->nullable();
            $table->foreign('song_search_id')->references('id')->on('songs_search');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs_search');
        Schema::dropIfExists('songs_pages');
    }
}
